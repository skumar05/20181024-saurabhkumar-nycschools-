//
//  SharedHelper.swift
//  NYCOpenData
//
//  Created by Saurabh Kumar on 11/9/17.
//  Copyright © 2018 Saurabh Kumar. All rights reserved.
//

import Foundation
import UIKit

class SharedHelper {
    
    static let sharedHelperInstance = SharedHelper()
    
    let activityView:UIActivityIndicatorView = UIActivityIndicatorView()
    
    func showLoading(view:UIView)
    {
        activityView.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        activityView.color = UIColor.black
        activityView.center = view.center
        activityView.hidesWhenStopped = true
        activityView.startAnimating()
        
        view.addSubview(activityView)
    }
    
    func removeLoading()
    {
        activityView .removeFromSuperview()
    }
}
