//
//  DetailVCTableViewCell.swift
//  NYCOpenData
//
//  Created by Saurabh Kumar.
//  Copyright © 2018 Saurabh Kumar. All rights reserved.
//

import UIKit

class DetailVCTableViewCell : UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailsLabel: UILabel!
    @IBOutlet weak var sliderView: UIView!
    var circularSlider : CircularSlider!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        addSliderView()
    }
    
    func addSliderView() {
        // init slider view
        let frame = CGRect(x: 0, y: 2, width: sliderView.frame.width, height: sliderView.frame.height-2.0)
        circularSlider = CircularSlider(frame: frame)
        
        // NOTE: sliderMaximumAngle must be set before currentValue
        circularSlider.isUserInteractionEnabled = false
        circularSlider.maximumAngle = 270.0
        circularSlider.unfilledArcLineCap = .round
        circularSlider.filledArcLineCap = .round
        //circularSlider.currentValue = 10
        circularSlider.lineWidth = 15
        
        // add to view
        sliderView.addSubview(circularSlider)
        
        // NOTE: create and set a transform to rotate the arc so the white space is centered at the bottom
        circularSlider.transform = circularSlider.getRotationalTransform()
        
    }
}
