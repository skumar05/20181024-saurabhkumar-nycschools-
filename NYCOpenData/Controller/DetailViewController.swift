//
//  DetailViewController.swift
//  NYCOpenData
//
//  Created by Saurabh Kumar 
//  Copyright © 2018 Saurabh Kumar. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    //Constants
    let SCHOOLDETAIL_URL = "https://data.cityofnewyork.us/resource/734v-jeq5.json"

    var schoolDBN = " "
    var schoolName = " "
    var dataSourceArray:NSArray?
    var dictKeyArray : NSArray = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print ("School DBN=:\(schoolDBN)")
        print ("School Name=:\(schoolName)")
        self.title = schoolName
        self.dataSourceArray = NSArray()
        SharedHelper.sharedHelperInstance.showLoading(view: self.view)
        getSchoolDetails(school_DBN:schoolDBN) // fetch School list from Web API
    }

    override func viewWillAppear(_ animated: Bool) {
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return (self.dataSourceArray?.count)!
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 150.0
//    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailVCTableViewCell", for: indexPath) as! DetailVCTableViewCell
        
        // Configure the cell...
        cell.titleLabel.text = dictKeyArray[indexPath.row] as? String
        
        cell.detailsLabel.text = self.dataSourceArray![indexPath.row] as? String
        cell.circularSlider.currentValue = Float(self.dataSourceArray![indexPath.row] as! String)!
        
        return cell
    }

    //MARK:- Web API functions
    
    func getSchoolDetails(school_DBN: String) {
        print(#function)
        
        let urlString = SCHOOLDETAIL_URL + "?dbn=" + school_DBN
        let url = URL(string:urlString)
        var responseArray:NSArray = NSArray()
        
        var dictionary:NSDictionary = NSDictionary()
        
        var dictValuesArray : NSArray = NSArray()
        
        URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
            
            if(error != nil)
            {
                print(error.debugDescription)
            }
            else
            {
                do{
                    let json = try JSONSerialization.jsonObject(with: data!, options:.allowFragments)
                    responseArray = json as! NSArray
                    
                    print("Printing Response array without using dumping : \(responseArray as AnyObject)")
                    
                    if responseArray.count > 0 {
                    dictionary = responseArray[0] as! NSDictionary //Since the response for details contains only one school, and we want that info in the detail view

                    let mutableDict : NSMutableDictionary = NSMutableDictionary(dictionary: dictionary)
                    let removeObjectForKeys : NSArray = ["school_name","num_of_sat_test_takers","dbn"] // Keys to filter out the details showing in screen
                    mutableDict .removeObjects(forKeys: removeObjectForKeys as! [Any])
                    
                    self.dictKeyArray = mutableDict.allKeys as NSArray
                    let staticKeys : NSArray = ["SAT Critical Reading average score","SAT Math average score","SAT Writing average score"]
                    self.dictKeyArray = staticKeys

                    //self.dictKeyArray = mutableDict.allKeys as NSArray
                    dictValuesArray = mutableDict.allValues as NSArray
                    
                    self.dataSourceArray = dictValuesArray
                    
                    OperationQueue.main.addOperation({
                        self.tableView .reloadData()
                        SharedHelper.sharedHelperInstance.removeLoading()
                        
                    })
                    }
                    else {
                        OperationQueue.main.addOperation({
                            SharedHelper.sharedHelperInstance.removeLoading()
                            
                            let myalert = UIAlertController(title: "School details not found", message: "", preferredStyle: UIAlertControllerStyle.alert)
                            myalert.addAction(UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                //print("OK Tapped ")
                                self.view.window?.rootViewController?.dismiss(animated: false, completion: nil)
                            })
                            self.present(myalert, animated: true)
                            
                        })
                    }
                    
                }catch let error as NSError{
                    print(error)
                    SharedHelper.sharedHelperInstance.removeLoading()
                }
            }
            
        }).resume()
    }


}


