//
//  ViewController.swift
//  NYCOpenData
//
//  Created by Saurabh Kumar
//  Copyright © 2018 Saurabh Kumar. All rights reserved.
//

import UIKit

class ListViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    //Constants 
    let LISTOFSCHOOLS_URL = "https://data.cityofnewyork.us/resource/97mf-9njv.json"
    
    //Variables
    var selectedSchoolDBN:String? //Selected's schools DBN
    var dataSourceArray:NSArray?
    var dataSourceArrayCopy : NSArray?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.dataSourceArray = NSArray()
        self.dataSourceArrayCopy = NSArray()
        self.title = "NYC Schools"
        //@SK Commenthere
        SharedHelper.sharedHelperInstance.showLoading(view: self.view)
        
        getSchoolList() // fetch School list from Web API

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Search bar Functions
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.endEditing(false)
        self.dataSourceArray = self.dataSourceArrayCopy
        self.tableView .reloadData()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        filterContentForSearchText(searchBarText: searchBar.text!)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.endEditing(false)
    }
    
     func filterContentForSearchText(searchBarText:String)
     {
        let  resultPredicate : NSPredicate = NSPredicate.init(format: "school_name CONTAINS [cd] %@", searchBarText)
        let filteredArray = dataSourceArray?.filtered(using: resultPredicate)
        
        //print(filteredArray!)
        
        let filteredNSArray:NSArray = filteredArray! as NSArray
        
        self.dataSourceArray = filteredNSArray
        self.tableView .reloadData()
    }
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return (self.dataSourceArray?.count)!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListVCTableViewCell", for: indexPath) as! ListVCTableViewCell
        
        // Configure the cell...
        //@SK Comment Needed
        let rowDict : NSDictionary = self.dataSourceArray![indexPath.row] as! NSDictionary
        cell.labelForCell.text = (rowDict["school_name"] as! String)
        //selectedSchoolDBN = (rowDict["dbn"] as! String) //@SK Remove this
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(#function)
        
        self.performSegue(withIdentifier: "goToSchoolDetails", sender: indexPath)
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
    }

    //MARK:- Web API functions
    
    func getSchoolList() {
     
        print(#function)
        
        let url = URL(string:LISTOFSCHOOLS_URL)
        
        var responseArray:NSArray = NSArray()
        
        URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
            
            if(error != nil)
            {
                print(error.debugDescription)
            }
            else
            {
                do{
                    let json = try JSONSerialization.jsonObject(with: data!, options:.allowFragments)
                    responseArray = json as! NSArray
                    
                    //print("Printing Response array without using dumping : \(responseArray as AnyObject)")
                    self.dataSourceArrayCopy = responseArray
                    self.dataSourceArray = responseArray
                    
                    OperationQueue.main.addOperation({
                        self.tableView .reloadData()
                        SharedHelper.sharedHelperInstance.removeLoading()
                        
                    })
                    
                    
                }catch let error as NSError{
                    print(error)
                    SharedHelper.sharedHelperInstance.removeLoading()
                }
            }
            
        }).resume()
    }

    //MARK: - Select School
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "goToSchoolDetails" {
            
            let selctedIndexPath = sender as! IndexPath
            let rowDict : NSDictionary = self.dataSourceArray![selctedIndexPath.row] as! NSDictionary
            print ("Selected School dbn=:\((rowDict["dbn"] as! String))")

            let detailVC = segue.destination as! DetailViewController
            detailVC.schoolDBN  = (rowDict["dbn"] as! String)
            detailVC.schoolName = (rowDict["school_name"] as! String)

         }
    }
}

//MARK: - TableView Cell Custom Class declaration ----
class ListVCTableViewCell: UITableViewCell {
    @IBOutlet weak var labelForCell: UILabel!
}



